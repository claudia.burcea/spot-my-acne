# -*- coding: utf-8 -*-
"""Acne_Detection_diploma_project

Automatically generated by Colab.

Original file is located at
    https://colab.research.google.com/drive/1NFn-1w6597Hm5JFnH9sSteSvOI7QskxW
"""

import os
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
import matplotlib.pyplot as plt

from tensorflow.image import resize

from tensorflow.keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
from tensorflow.keras.applications import MobileNet
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, GlobalAveragePooling2D
from tensorflow.keras.optimizers.legacy import Adam
from tensorflow.keras.utils import to_categorical
from keras.preprocessing import image
import matplotlib.pyplot as plt
from PIL import Image


from sklearn.metrics import precision_recall_fscore_support, accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split

from google.colab import drive
drive.mount('/content/drive')

def read_xlsx_annotations(xlsx_path, images_dir):
    df = pd.read_excel(xlsx_path)

    df = df.dropna(subset=['filename'])
    df['filename'] = df['filename'].astype(str)

    image_paths = [os.path.join(images_dir, filename) for filename in df['filename'].tolist()]
    acne_types = df['class'].tolist()

    return image_paths, acne_types

acne_type_dir= '/content/drive/MyDrive/Acne_classification/Acne_type'

train_image_paths, train_acne_types = read_xlsx_annotations(os.path.join(acne_type_dir, 'train_annotations.xlsx'), os.path.join(acne_type_dir, 'Train'))
valid_image_paths, valid_acne_types = read_xlsx_annotations(os.path.join(acne_type_dir, 'valid_annotations.xlsx'), os.path.join(acne_type_dir, 'Valid'))
test_image_paths, test_acne_types = read_xlsx_annotations(os.path.join(acne_type_dir, 'test_annotations.xlsx'), os.path.join(acne_type_dir, 'Test'))
print(train_acne_types)

acne_type_labels = sorted(set(train_acne_types + valid_acne_types + test_acne_types))
acne_type_map = {label: i for i, label in enumerate(acne_type_labels)}
train_acne_types = [acne_type_map[label] for label in train_acne_types]
valid_acne_types = [acne_type_map[label] for label in valid_acne_types]
test_acne_types = [acne_type_map[label] for label in test_acne_types]
"""
print(train_acne_types)
print(acne_type_labels)
print(acne_type_map)
print(len(train_acne_types))
print(len(train_image_paths))
print(np.unique(train_acne_types))
print(valid_acne_types)
print(test_acne_types)
"""

def list_all_image_paths(directory):
    image_paths = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif')):
                image_paths.append(os.path.join(root, file))
    return image_paths

acne_type_train_dir = os.path.join(acne_type_dir, 'Train')

all_image_paths = list_all_image_paths(acne_type_train_dir)

for path in all_image_paths:
    print(path)

#print(f"Total number of images in {acne_type_train_dir}: {len(all_image_paths)}")

"""train_acne_types = np.array(train_acne_types)
valid_acne_types = np.array(valid_acne_types)
test_acne_types = np.array(test_acne_types)

print(train_acne_types)
print(valid_acne_types)
print(test_acne_types)
"""

def load_and_preprocess_image(image_path, target_size=(224, 224)):
    try:
        img = load_img(image_path,  target_size=target_size)
        img_array = img_to_array(img)
        return img_array / 255.0
    except Exception as e:
        print(f"Error loading image {image_path}: {e}")
        return None

#print("Number of unique acne type labels:", len(acne_type_labels))
#print(acne_type_labels)

def data_generator_type(image_paths, acne_type_labels, batch_size=32, target_size=(224, 224)):
    while True:
        for start in range(0, len(image_paths), batch_size):
            end = min(start + batch_size, len(image_paths))
            batch_image_paths = image_paths[start:end]
            batch_acne_type_labels = acne_type_labels[start:end]
            batch_images = np.array([load_and_preprocess_image(img_path) for img_path in batch_image_paths])
            batch_acne_type_labels_cat = to_categorical(batch_acne_type_labels, num_classes=6)
            yield batch_images, {
                'acne_type_output': batch_acne_type_labels_cat
            }

batch_size = 32

train_generator_type = data_generator_type(train_image_paths, train_acne_types, batch_size)
val_generator_type = data_generator_type(valid_image_paths, valid_acne_types)

batch_images, batch_labels = next(train_generator_type)
print(f"Batch images shape: {batch_images.shape}")
print(f'First batch_labels[\'acne_type_output\'].shape: {batch_labels["acne_type_output"].shape}')

mobilenet = MobileNet(weights='imagenet', include_top=False, input_shape=(224, 224, 3))
for layer in mobilenet.layers:
    layer.trainable = False

x = mobilenet.output
x = GlobalAveragePooling2D()(x)
acne_type_output = Dense(6, activation='softmax', name='acne_type_output')(x)

model = Model(inputs=mobilenet.input, outputs=[acne_type_output])

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
    loss={
        'acne_type_output': 'categorical_crossentropy'
    },
    metrics=['accuracy']
)

"""print("Unique acne type labels in training data:", np.unique(train_acne_types))
print("Unique acne type labels in validation data:", np.unique(valid_acne_types))
print("Unique acne type labels in test data:", np.unique(test_acne_types))

print("Length of train_image_paths:", len(train_image_paths))
print("Length of train_acne_types:", len(train_acne_types))
print(len(train_acne_types))
print()
"""

def count_images_in_directory(directory):
    return sum([len(files) for root, dirs, files in os.walk(directory)])

acne_type_train_dir = os.path.join(acne_type_dir, 'Train')

acne_type_train_count = count_images_in_directory(acne_type_train_dir)

#print("Number of images in Acne Type Train directory:", acne_type_train_count)

history = model.fit(
    train_generator_type,
    steps_per_epoch=len(train_image_paths) // 32,
    epochs=20,
    validation_data=val_generator_type,
    validation_steps=len(valid_image_paths) // 32
)

model.save('type_detection_model_15_epochs.h5')

acne_type = tf.keras.models.load_model('severity_detection_model_20_epochs.h5')
from keras.models import load_model

severity_detection_model_20_epochs = load_model("severity_detection_model_20_epochs.h5")

# Convertire
converter = tf.lite.TFLiteConverter.from_keras_model(severity_detection_model_20_epochs)
tflite_model = converter.convert()
print("converted")

with open('severity_detection_model_20_epochs.tflite', 'wb') as f:
  f.write(tflite_model)

def load_severity_data(severity_dir):
    severity_labels = ["Mild", "Moderate", "Severe", "Very Severe"]
    severity_map = {label: i for i, label in enumerate(severity_labels)}

    image_paths_train = []
    severity_types_train = []
    image_paths_valid = []
    severity_types_valid = []
    image_paths_test = []
    severity_types_test = []

    for split in ['Train', 'Valid', 'Test']:
        split_dir = os.path.join(severity_dir, split)
        for severity_label in severity_labels:
            severity_label_dir = os.path.join(split_dir, severity_label)
            image_paths = []
            severity_types = []
            for img_name in os.listdir(severity_label_dir):
                image_path = os.path.join(severity_label_dir, img_name)
                image_paths.append(image_path)
                severity_types.append(severity_map[severity_label])
            if split == 'Train':
                image_paths_train.extend(image_paths)
                severity_types_train.extend(severity_types)
            elif split == 'Valid':
                image_paths_valid.extend(image_paths)
                severity_types_valid.extend(severity_types)
            elif split == 'Test':
                image_paths_test.extend(image_paths)
                severity_types_test.extend(severity_types)

    return image_paths_train, severity_types_train, image_paths_valid, severity_types_valid, image_paths_test, severity_types_test

severity_dir = '/content/drive/MyDrive/Acne_classification/Severity'

train_severity_paths, train_severity_labels, valid_severity_paths, valid_severity_labels, test_severity_paths, test_severity_labels = load_severity_data(severity_dir)

severity_labels = ["Mild", "Moderate", "Severe", "Very Severe"]
severity_map = {label: i for i, label in enumerate(severity_labels)}
#train_severity = [severity_map[label] for label in train_severity_labels]
#valid_severity = [severity_map[label] for label in valid_severity_labels]
#severity_types = [severity_map[label] for label in test_severity_labels]
"""
print(severity_map)

print(train_severity_paths)
print(train_severity_labels)

print(valid_severity_paths)
print(valid_severity_labels)

print(test_severity_paths)
print(test_severity_labels)"""

train_severity_labels = np.array(train_severity_labels)
valid_severity_labels = np.array(valid_severity_labels)

"""print(train_severity_labels)
print(valid_severity_labels)

print("Number of unique severity labels:", len(severity_labels))
print(severity_labels)"""

def data_generator_severity(image_paths, severity_labels, batch_size=32, target_size=(224, 224)):
    while True:
        for start in range(0, len(image_paths), batch_size):
            end = min(start + batch_size, len(image_paths))
            batch_image_paths = image_paths[start:end]
            batch_severity_labels = severity_labels[start:end]
            batch_images = np.array([load_and_preprocess_image(img_path) for img_path in batch_image_paths])
            batch_severity_labels_cat = to_categorical(batch_severity_labels, num_classes=4)
            yield batch_images, {
                'severity_output': batch_severity_labels_cat,
            }

def train_val_generators(training_dir, validation_dir, test_dir):

    train_datagen = ImageDataGenerator(rescale=1./255.)

    train_generator = train_datagen.flow_from_directory(directory=training_dir,
                                                        batch_size=16,
                                                        class_mode='categorical',
                                                        target_size=(224, 224))

    validation_datagen = ImageDataGenerator(rescale=1./255.)

    validation_generator = validation_datagen.flow_from_directory(directory=validation_dir,
                                                                  batch_size=16,
                                                                  class_mode='categorical',
                                                                  target_size=(224, 224))

    test_datagen = ImageDataGenerator(rescale=1./255.)

    test_generator = test_datagen.flow_from_directory(directory=test_dir,
                                                                  batch_size=16,
                                                                  class_mode='categorical',
                                                                  target_size=(224, 224))

    return train_generator, validation_generator, test_generator

training_dir = '/content/drive/MyDrive/Acne_classification/Severity/Train'
validation_dir = '/content/drive/MyDrive/Acne_classification/Severity/Valid'
test_dir = '/content/drive/MyDrive/Acne_classification/Severity/Test'
train_generator_severity, valid_generator_severity, test_generator_severity = train_val_generators(training_dir, validation_dir, test_dir)

batch_size = 32

train_generator_severity = data_generator_severity(train_severity_paths, train_severity_labels, batch_size)
val_generator_severity = data_generator_severity(valid_severity_paths, valid_severity_labels)

batch_images, batch_labels = next(train_generator_severity)
print(f"Batch images shape: {batch_images.shape}")
print(f'First batch_labels[\'severity_output\'].shape: {batch_labels["severity_output"].shape}')

mobilenet = MobileNet(weights='imagenet', include_top=False, input_shape=(224, 224, 3))
for layer in mobilenet.layers:
    layer.trainable = False

x = mobilenet.output
x = GlobalAveragePooling2D()(x)
severity_output = Dense(4, activation='softmax', name='severity_output')(x)

model = Model(inputs=mobilenet.input, outputs=[severity_output])

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
    loss={
        'severity_output': 'categorical_crossentropy',
    },
    metrics=['accuracy']
)

"""
print("Unique severity labels in training data:", np.unique(train_severity_labels))
print("Unique severity labels in validation data:", np.unique(valid_severity_labels))

print("Length of train_image_paths:", len(train_severity_paths))
print("Length of train_severity_labels:", len(train_severity_labels))
"""

def count_images_in_directory(directory):
    return sum([len(files) for root, dirs, files in os.walk(directory)])

mild_train_dir = '/content/drive/MyDrive/Acne_classification/Severity/Train'
severity_train_count = count_images_in_directory(mild_train_dir)
#print("Number of images in Severity Train directory:", severity_train_count)

history = model.fit(
    train_generator_severity,
    steps_per_epoch=len(train_severity_paths) // 32,
    epochs=60,
    validation_data=val_generator_severity,
    validation_steps=len(valid_severity_paths) // 32,
)

model.save('severity_detection_model_60_epochs.h5')

severity = tf.keras.models.load_model('severity_detection_model_60_epochs.h5')

#print(train_severity_labels)

acne_type = tf.keras.models.load_model('type_detection_model_15_epochs.h5')

img_path = '/content/drive/MyDrive/Acne_classification/Severity/Test/Severe/levle2_176.jpg'

img = image.load_img(img_path, target_size=(224, 224))
img_array = image.img_to_array(img)
img_array = np.expand_dims(img_array, axis=0)
img_array /= 255.0

predictionss = severity.predict(img_array)
class_labelss = ['Mild', 'Moderate', 'Severe', 'Very Severe']
severity_predicted_class = np.argmax(predictionss, axis=1)

#print("Predicted class:", class_labelss[severity_predicted_class[0]])

"""img_path = '/content/drive/MyDrive/Acne_classification/Acne_type/Test/rotated_acne-pustular-58_jpg.rf.e0e4955fa02010c7b40671f9abe9c14c.jpg'

img = image.load_img(img_path, target_size=(224, 224))
img_array = image.img_to_array(img)
img_array = np.expand_dims(img_array, axis=0)
img_array /= 255.0

# predicția
#predictions = severity.predict(img_array)
#class_labels = ['Blackheads', 'Cysts', 'Nodules', 'Papules', 'Pustules', 'Whitehead']

# clasa cu probabilitatea cea mai mare
predicted_classes = [class_labels[i] for i, prob in enumerate(predictions[0]) if prob > 0.1]
if predicted_classes:
    print("Predicted classes:", predicted_classes)
else:
    print("No classes exceed the threshold.")

# clasa prezisă
print("Predicted probabilities for each class:")
for label, prob in zip(class_labels, predictions[0]):
    print(f"{label}: {prob:.4f}")


predictionss = severity.predict(img_array)
class_labelss = ['Mild', 'Moderate', 'Severe', 'Very Severe']
severity_predicted_class = np.argmax(predictionss, axis=1)

print("Predicted class:", class_labelss[severity_predicted_class[0]])

print(len(test_severity_paths))
print(test_severity_paths)
print(len(test_severity_labels))
print(test_severity_labels)
print(len(test_image_paths))
print(len(test_acne_types))
print(test_image_paths)
print(test_acne_types)

print(severity_labels)
print(severity_map)
print(acne_type_map)
"""

model = tf.keras.models.load_model('severity_detection_model_60_epochs.h5')

def load_and_preprocess_image(image_path, target_size=(224, 224)):
    try:
        img = load_img(image_path, target_size=target_size)
        img_array = img_to_array(img)
        img_array = np.expand_dims(img_array, axis=0)
        return img_array / 255.0
    except Exception as e:
        print(f"Error loading image {image_path}: {e}")
        return None

def get_predictions(model, image_paths):
    predictions = []
    #probabilities = []

    for path in image_paths:
        img = load_and_preprocess_image(path)
        if img is not None:
            prediction = model.predict(img)
            #probabilities.append(prediction)
            predicted_class = np.argmax(prediction, axis=1)[0]
            predictions.append(predicted_class)
    return predictions#, probabilities

predicted_classes = get_predictions(model, test_severity_paths)
true_classes = test_severity_labels
print(len(predicted_classes))
print(len(true_classes))


cm = confusion_matrix(true_classes, predicted_classes)
class_labels = sorted(severity_map.keys(), key=lambda x: severity_map[x])

plt.figure(figsize=(10, 8))
sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", xticklabels=class_labels, yticklabels=class_labels)
plt.xlabel('Predicted')
plt.ylabel('True')
plt.title('Confusion Matrix')
plt.show()

#print(classification_report(true_classes, predicted_classes, target_names=class_labels))

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix, classification_report, roc_curve, auc
from sklearn.preprocessing import label_binarize
from tensorflow.keras.preprocessing.image import load_img, img_to_array
import tensorflow as tf

# Obține predicțiile modelului asupra setului de date de test
def get_predictions(model, image_paths):
    predictions = []
    probabilities = []
    for path in image_paths:
        img = load_and_preprocess_image(path)
        if img is not None:
            prediction = model.predict(img)
            probabilities.append(prediction)
            predicted_class = np.argmax(prediction, axis=1)[0]
            predictions.append(predicted_class)
    return predictions, probabilities

predicted_classes, probabilities = get_predictions(model, test_severity_paths)
true_classes = test_severity_labels

# Binarizarea etichetelor reale pentru calcularea ROC și AUC
true_classes_bin = label_binarize(true_classes, classes=class_labels)
n_classes = len(class_labels)
probabilities_transposed = np.transpose(probabilities, axes=(0, 2, 1))[:, :, 0]

# Calculate ROC and AUC for each class
fpr = dict()
tpr = dict()
roc_auc = dict()
for i in range(n_classes):
    fpr[i], tpr[i], _ = roc_curve(true_classes_bin[:, i], probabilities_transposed[:, i])
    roc_auc[i] = auc(fpr[i], tpr[i])

# Plot ROC Curve for each class
plt.figure(figsize=(8, 6))
colors = ['aqua', 'darkorange', 'cornflowerblue']  # Adjust colors as needed
for i, color in zip(range(n_classes), colors):
    plt.plot(fpr[i], tpr[i], color=color, lw=2,
             label='ROC curve of class {0} (area = {1:0.2f})'
             ''.format(class_labels[i], roc_auc[i]))

plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.xlim([0.0, 10.0])
plt.ylim([0.0, 10.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic (ROC) Curve')
plt.legend(loc="lower right")
plt.grid(True)
plt.show()

from sklearn.metrics import  classification_report

print(classification_report(true_classes, predicted_classes, target_names=class_labels))

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.legend(['Training', 'Validation'], loc='upper left')
plt.show()

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model Loss')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.legend(['Training', 'Validation'], loc='upper left')
plt.show()