 **Spot My Acne**



Adresa repository-ului pe gitlab.upt.ro: https://gitlab.upt.ro/claudia.burcea/spot-my-acne.git


Instalare:
1. Se instalează Android Studio pe un PC sau laptop.
2. Se deschide în acest mediu de programare codul sursă de la adresa repository-ului.
3. Din Device Manager se creează un device virtual.
4. Din tab-ul Tools, se configurează baza de date Firebase pentru autentificare si Firestore Database.

Compilare:
1. Se sincronizează proiectul.
2. Se pornește device-ul virtual.
3. Se dă build project.
4. Se lansează aplicația și se poate utiliza.

Se poate folosi și un smartphone fizic, în loc de un device virtual. Acesta trebuie conectat la PC sau laptop printr-un cablu.

În fișierul de la calea Cod sursă modelele antrenate/acne_detection_diploma_project.py este codul sursă pentru modelele antrenate.

La adresa https://drive.google.com/drive/folders/16ROAqqitUpfMCh7I9Apmup2AJo23laY0 este setul de date pe care am antrenat modelele.

