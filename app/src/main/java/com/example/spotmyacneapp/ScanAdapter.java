package com.example.spotmyacneapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;
import java.util.Objects;

public class ScanAdapter extends ArrayAdapter<Scan> {
    private final Context context;
    private final List<Scan> scanList;

    public ScanAdapter(Context context, List<Scan> scanList) {
        super(context, R.layout.scan_item, scanList);
        this.context = context;
        this.scanList = scanList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.scan_item, parent, false);
        }

        Scan scan = scanList.get(position);

        ImageView imageView = convertView.findViewById(R.id.scan_image);
        TextView predictionTextView = convertView.findViewById(R.id.scan_prediction);
        TextView resultsTextView = convertView.findViewById(R.id.scan_results);
        TextView severityTextView = convertView.findViewById(R.id.scan_severity);
        Button deleteButton = convertView.findViewById(R.id.delete_button);

        predictionTextView.setText(scan.getPrediction());
        resultsTextView.setText(scan.getResults());
        severityTextView.setText(scan.getSeverity());

        Picasso.get().load(scan.getImageUrl()).into(imageView);

        deleteButton.setOnClickListener(v -> deleteScan(position));

        return convertView;
    }

    private void deleteScan(int position) {
        Scan scan = scanList.get(position);
        FirebaseFirestore fStore = FirebaseFirestore.getInstance();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        String userId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        fStore.collection("users").document(userId).collection("scans")
                .document(scan.getId())
                .delete()
                .addOnSuccessListener(aVoid -> {
                    Toast.makeText(context, "Scan deleted successfully.", Toast.LENGTH_SHORT).show();
                    scanList.remove(position);
                    notifyDataSetChanged();
                })
                .addOnFailureListener(e -> Toast.makeText(context, "Failed to delete scan.", Toast.LENGTH_SHORT).show());
    }
}
