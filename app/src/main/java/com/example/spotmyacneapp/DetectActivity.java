package com.example.spotmyacneapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

//import com.example.spotmyacneapp.ml.TypeDetectionModel15Epochs;
import com.example.spotmyacneapp.ml.SeverityDetectionModel20Epochs;
import com.example.spotmyacneapp.ml.TypeDetectionModel20Epochs;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
//import java.util.Objects;

public class DetectActivity extends AppCompatActivity {
    //private static final int CAMERA_REQUEST_CODE = 102;
    //private static final int CAMERA_PERM_CODE = 101;
    Button camera;
    Button gallery;
    Button scan;
    Button save;
    ImageButton back;
    ImageView selectedImage;
    TextView type_result;
    TextView procents_type;
    TextView severity_result;
    Bitmap bitmap;

    private FirebaseAuth mAuth;
    private FirebaseFirestore fStore;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detect);

        mAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        back = findViewById(R.id.back_detect);
        camera = findViewById(R.id.camera);
        gallery = findViewById(R.id.gallery);
        save = findViewById(R.id.save);
        selectedImage = findViewById(R.id.image_frame);
        type_result = findViewById(R.id.msg_for_detection);
        procents_type = findViewById(R.id.textViewResult2);
        severity_result = findViewById(R.id.severity);
        scan = findViewById(R.id.scan);


        back.setOnClickListener(view -> {
            Intent intent = new Intent(DetectActivity.this, HomeActivity.class);
            startActivity(intent);
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, 10);
            }
        });

        getPermission();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 12);
            }
        });

        scan.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {

// scan for type
                try {
                    TypeDetectionModel20Epochs model = TypeDetectionModel20Epochs.newInstance(DetectActivity.this);

                    TensorBuffer inputFeature0 = TensorBuffer.createFixedSize(new int[]{1, 224, 224, 3}, DataType.FLOAT32);
                    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4*224*224*3);
                    byteBuffer.order(ByteOrder.nativeOrder());

                    int [] intValues = new int[224*224];
                    bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
                    int pixel = 0;
                    for(int i = 0; i < 224; i++){
                        for (int j = 0; j < 224; j++){
                            int val = intValues[pixel++]; //RGB
                            byteBuffer.putFloat(((val>>16)&0xFF)*(1.f/255.f));
                            byteBuffer.putFloat(((val>>8)&0xFF)*(1.f/255.f));
                            byteBuffer.putFloat((val&0xFF)*(1.f/255.f));
                        }
                    }
                    inputFeature0.loadBuffer(byteBuffer);

                    TypeDetectionModel20Epochs.Outputs outputs = model.process(inputFeature0);
                    TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();

                    float[] confidences = outputFeature0.getFloatArray();
                    int maxPos = 0;
                    float maxConfidence = 0;
                    for(int i = 0; i < confidences.length; i++){
                        if(confidences[i] > maxConfidence){
                            maxConfidence = confidences[i];
                            maxPos = i;
                        }
                    }

                    String[] classes = {"Blackheads", "Whitehead", "Cysts", "Nodules", "Papules", "Pustules"};

                    type_result.setText("The most proeminent are " + classes[maxPos] + ".");

                    StringBuilder s = new StringBuilder();
                    for(int i = 0; i < classes.length; i++){
                        s.append(String.format(Locale.getDefault(), "%s: %.1f%%\n", classes[i], confidences[i] * 100));
                    }
                    procents_type.setText(s.toString());
                    model.close();

                } catch (IOException e) {
                    //  Handle the exception
                }
//scan for severity
                try {
                    SeverityDetectionModel20Epochs model = SeverityDetectionModel20Epochs.newInstance(DetectActivity.this);

                    // Creates inputs for reference.
                    TensorBuffer inputFeature0 = TensorBuffer.createFixedSize(new int[]{1, 224, 224, 3}, DataType.FLOAT32);
                    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4*224*224*3);
                    byteBuffer.order(ByteOrder.nativeOrder());

                    int [] intValues = new int[224*224];
                    bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
                    int pixel = 0;
                    for(int i = 0; i < 224; i++){
                        for (int j = 0; j < 224; j++){
                            int val = intValues[pixel++]; //RGB
                            byteBuffer.putFloat(((val>>16)&0xFF)*(1.f/255.f));
                            byteBuffer.putFloat(((val>>8)&0xFF)*(1.f/255.f));
                            byteBuffer.putFloat((val&0xFF)*(1.f/255.f));
                        }
                    }
                    inputFeature0.loadBuffer(byteBuffer);

                    SeverityDetectionModel20Epochs.Outputs outputs = model.process(inputFeature0);
                    TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();

                    float[] confidences = outputFeature0.getFloatArray();
                    int maxPos = 0;
                    float maxConfidence = 0;
                    for(int i = 0; i < confidences.length; i++){
                        if(confidences[i] > maxConfidence){
                            maxConfidence = confidences[i];
                            maxPos = i;
                        }
                    }

                    String[] classes = {"Mild", "Moderate", "Severe", "Very Severe"};

                    severity_result.setText("Level of severity: " + classes[maxPos]);


                    // Releases model resources if no longer used.
                    model.close();
                }catch (IOException e) {
                    //  Handle the exception
                }

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if (currentUser == null) {
                    Toast.makeText(DetectActivity.this, "You need to be logged in to save a scan.", Toast.LENGTH_SHORT).show();
                    new AlertDialog.Builder(DetectActivity.this)
                            .setTitle("Not Logged In")
                            .setMessage("You need to be logged in to save a scan. Do you want to log in now?")
                            .setPositiveButton("Log In", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Open MainActivity for login
                                    Intent intent = new Intent(DetectActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton("Cancel", null)
                            .show();
                } else {
                    String userId = currentUser.getUid();
                    saveScanToFirebase(userId);
                }
            }
        });
    }

    private void saveScanToFirebase(String userId) {
        if (bitmap == null) {
            Toast.makeText(this, "No image selected.", Toast.LENGTH_SHORT).show();
            return;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageData = baos.toByteArray();

        String prediction = type_result.getText().toString();
        String results = procents_type.getText().toString();
        String severityText = severity_result.getText().toString();

        // Create a new document with a generated ID
        DocumentReference scansRef = fStore.collection("users").document(userId).collection("scans").document();

        // Upload image to Firestore Storage
        scansRef.set(new HashMap<>()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                scansRef.update("prediction", prediction,
                        "results", results,
                        "severity", severityText).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Upload image to Firebase Storage
                        String imageName = scansRef.getId() + ".jpg";
                        StorageReference imageRef = FirebaseStorage.getInstance().getReference()
                                .child("scans/" + userId + "/" + imageName);

                        UploadTask uploadTask = imageRef.putBytes(imageData);
                        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        scansRef.update("imageUrl", uri.toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(DetectActivity.this, "Scan saved successfully.", Toast.LENGTH_SHORT).show();
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(DetectActivity.this, "Failed to save image URL.", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(DetectActivity.this, "Failed to upload image.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(DetectActivity.this, "Failed to save scan details.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getPermission() {
        if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DetectActivity.this, new String[]{Manifest.permission.CAMERA}, 11);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==11){
            if(grantResults.length>0){
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    this.getPermission();
                }
            }
        }
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==10){
            if(data!=null){
                Uri uri = data.getData();
                try{
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    bitmap = Bitmap.createScaledBitmap(bitmap, 224, 224, true);

                    selectedImage.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else if (requestCode==12){
            assert data != null;
            bitmap = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            if (bitmap != null) {
                bitmap = Bitmap.createScaledBitmap(bitmap, 224, 224, true);
            }
            selectedImage.setImageBitmap(bitmap);
        }
        super.onActivityResult(requestCode,resultCode,data);
    }
}
