package com.example.spotmyacneapp;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ViewScansActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseFirestore fStore;
    private ScanAdapter scanAdapter;
    private List<Scan> scanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewscans);

        mAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ListView scansListView = findViewById(R.id.scans_list_view);
        scanList = new ArrayList<>();
        scanAdapter = new ScanAdapter(this, scanList);
        scansListView.setAdapter(scanAdapter);

        loadScans();
    }

    private void loadScans() {
        String userId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
        fStore.collection("users").document(userId).collection("scans")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot documents = task.getResult();
                        if (documents != null) {
                            for (DocumentSnapshot document : documents) {
                                String prediction = document.getString("prediction");
                                String results = document.getString("results");
                                String severity = document.getString("severity");
                                String imageUrl = document.getString("imageUrl");
                                String id = document.getId();

                                Scan scan = new Scan(prediction, results, severity, imageUrl, id);
                                scanList.add(scan);
                            }
                            scanAdapter.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(ViewScansActivity.this, "Failed to load scans.", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
