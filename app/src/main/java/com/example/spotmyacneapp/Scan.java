package com.example.spotmyacneapp;

public class Scan {
    private final String prediction;
    private final String results;
    private final String severity;
    private final String imageUrl;
    private String id;


    public Scan(String prediction, String results, String severity, String imageUrl, String id) {
        this.prediction = prediction;
        this.results = results;
        this.severity = severity;
        this.imageUrl = imageUrl;
        this.id = id;
    }

    public String getPrediction() {
        return prediction;
    }

    public String getResults() {
        return results;
    }

    public String getSeverity() {
        return severity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
