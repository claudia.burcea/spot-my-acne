package com.example.spotmyacneapp;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Objects;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("About Acne");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView acneTypesTextView = findViewById(R.id.acne_types);
        TextView severityTextView = findViewById(R.id.severity);

        String acneTypesDescription = "1. Blackheads: Small bumps that appear on your skin due to clogged hair follicles.\n" +
                "2. Whiteheads: Small white bumps that appear on the skin due to clogged pores.\n" +
                "3. Papules: Small red, raised bumps caused by inflamed or infected hair follicles.\n" +
                "4. Pustules: Red, tender bumps with white pus at their tips.\n" +
                "5. Nodules: Large, painful, solid lumps beneath the surface of the skin.\n" +
                "6. Cysts: Large, painful, pus-filled lumps beneath the surface of the skin.";

        String severityDescription = "Severity Levels:\n" +
                "1. Mild: Few occasional pimples.\n" +
                "2. Moderate: Inflammatory papules and pustules.\n" +
                "3. Severe: Numerous papules and pustules along with occasional inflamed nodules.\n" +
                "4. Very Severe: Numerous large, painful, and inflamed nodules and pustules.";

        acneTypesTextView.setText(acneTypesDescription);
        severityTextView.setText(severityDescription);


    }
}
