package com.example.spotmyacneapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.appcompat.app.AppCompatActivity;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    Button your_scans;
    Button check_your_acne;
    Button about;
    TextView logOut;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        your_scans = findViewById(R.id.your_scans);
        check_your_acne = findViewById(R.id.check_your_acne);
        about = findViewById(R.id.about);
        logOut = findViewById(R.id.logout);

        mAuth = FirebaseAuth.getInstance();

        your_scans.setOnClickListener(view -> {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser == null) {
                Toast.makeText(HomeActivity.this, "You need to be logged in to view your scans.", Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(HomeActivity.this, ViewScansActivity.class);
                startActivity(intent);
            }
        });

        check_your_acne.setOnClickListener(view -> {
            Intent intent = new Intent(HomeActivity.this, DetectActivity.class);
            Log.d("DetectActivity", "onCreate called");
            startActivity(intent);
        });

        logOut.setOnClickListener(view -> {
            mAuth.signOut();
            Intent intent = new Intent(HomeActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        });

        about.setOnClickListener(view -> {
            Intent intent = new Intent(HomeActivity.this, AboutActivity.class);
            startActivity(intent);
        });
    }
}

